namespace exercicio_3
{
    let livros: any[] =
    [
    {titulo: "Titulo 1", autor:"Autor1"},
    {titulo: "Titulo 2", autor:"Autor2"},
    {titulo: "Titulo 3", autor:"Autor3"},
    {titulo: "Titulo 4", autor:"Autor4"},
    {titulo: "Titulo 5", autor:"Autor5"},
];

let autores = livros.map((livro) => 
{
    return livro.autor
});
let titulos = livros.map((livro) => 
{
    return livro.titulo
});                            
console.log(autores);
console.log(titulos);

let autor3 = livros.filter((livro) => 
{
    return livro.autor == "Autor 3"
})
console.log(autor3);

}