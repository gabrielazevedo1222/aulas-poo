//Crie um vetor chamado "aluno" contendo três objetos, cada um representando um aluno coma aseguintes propriedades: "nome" (string), "idade" (number) e "notas" (array de números).Preencha o vetor com informações fictícias.
//Em seguida, percorra o vetor utilizando a função "forEach" e para cada aluno, cálcule a média das notas e imprima o resultado na tela, juntamento com o nome e a idade do aluno.
interface Aluno
    {
        nome: string,
        idade: number,
        notas: number[];
    }
namespace Exercicios_6
{
    const alunos: Aluno[] = 
    [
        {nome: "Rodrigo", idade: 20, notas:[4,7,8]},
        {nome: "Gabriel", idade: 23, notas:[2,5,10]},
        {nome: "Felipe", idade: 49, notas:[10,9,1]},
        {nome: "Victor", idade: 18, notas:[3,4,2]},
        {nome: "Rafael", idade: 33, notas:[1,10,5]},
    ]

    alunos.forEach((aluno) => 
    {
        let media = aluno.notas.reduce((total, nota) => {return total + nota}
        ) / aluno.notas.length
        if (media >= 6)
        {
            console.log(`A média do aluno: ${media} e está aprovado`);
        } 
        else 
        {
            console.log(`A média do aluno: ${aluno.nome} é igual ${media} e está reprovado`);
        }
    });
}