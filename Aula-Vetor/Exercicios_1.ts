namespace Exercicio_1

{
    let numeros: number [] = [10, 7, 5, 3, 1];
    let somaNumeros: number = 0;

    for(let i = 0; i < numeros.length; i++)
    {
        somaNumeros += numeros[i]
    }

    console.log(`O resultado da soma é: ${somaNumeros}`);

    //Criando uma interação com multiplicação
    let multi: number = 1;
    for (let i = 0; i < numeros.length; i++)
    {
        multi *= numeros[i];
    }
    
    console.log(`O resultado da multiplicação é:${multi}`);
}
