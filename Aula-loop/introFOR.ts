namespace introFOR
{
    for(let i = 0; i < 100; i++)
    {
        console.log(" esta é a interação numerica: " + i);
    }

    //mostrar todos os números impares
    for(let i = 0; i <= 100; i++)
    {
        if(i % 2 != 0)
        {
            console.log(`O número ${i} é impar`);
        }
    }
}