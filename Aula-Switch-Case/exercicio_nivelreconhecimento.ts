namespace  exercicio_nivelreconhecimento

{
    let nivel : string = "médio"
    
    switch(nivel)
    {
        case "alto" : console.log("Seu nível em TypeScript é alto!");
                break;
        case "médio" : console.log("Seu nível em TypeScript é médio!");
                break;
        case "baixo" : console.log ("Seu nível em TypeScript é baixo!");
                break;
        default : console.log("Você não sabe programar!");
    }
}
